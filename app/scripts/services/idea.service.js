angular.module("IdeaCanvas")
  .service("IdeaService", function(Idea){
    var self = {
      loadAll: function (cb) {
        var req = Idea.query({limit: 500});

        self.promise(req, cb);
      },
      create: function (obj, cb) {
        var req = Idea.save(obj);

        self.promise(req, cb);
      },
      promise: function(fn, cb){
        fn.$promise.then(cb);
      }
    };
    return self;
  });
