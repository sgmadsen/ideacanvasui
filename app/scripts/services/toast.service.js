angular.module("IdeaCanvas")
  .service("Toast", function($mdToast, Messages, $timeout){
    var self = {
      error: function (type) {
        var toast = $mdToast.simple()
          .textContent(Messages[type]['error']['content'])
          .action(Messages[type]['error']['action'])
          .highlightAction(true)
          .position("end bottom right");

        return {
          show: function () {
            $mdToast.show(toast).then(function(response) {
              if ( response == 'ok' ) {
                alert('better luck next time');
              }
            });
          },
          hide: function () {
            $mdToast.hide(toast);
          }
        }
      },
      login: {
        success: function (username, cb) {
          var toastContent = Messages['login']['success'];
          var toast = $mdToast.simple()
            .textContent(toastContent.content)
            .action(toastContent.action)
            .highlightAction(true)
            .position("end bottom right");

          $mdToast.show(toast).then(function (response) {
            if (response == 'ok') {
              cb();
            }
          });
        }
      },
      misc: {
        condescend: function (name, cb) {
          var toastContent = Messages['condescend'];
          var content = toastContent.part1 + name + toastContent.part2;
          var toast = $mdToast.simple()
            .textContent(content)
            .action(toastContent.action)
            .highlightAction(true)
            .position("end bottom right");

          $mdToast.show(toast).then(function (response) {
            if (response == 'ok') {
              cb();
            }
          });
        }
      }
    };

    return self;
  })
