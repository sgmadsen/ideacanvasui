angular.module("IdeaCanvas")
  .service("NavService", function($rootScope){
    var self = {
      hideTabs: function () {
        $rootScope.$broadcast("hideTabs");
      },
      showTabs: function () {
        $rootScope.$broadcast("showTabs");
      },
      setTabs: function (tabs) {
        $rootScope.tabs = tabs;
        $rootScope.$broadcast("setTabs");
      }
    };
    return self;
  });
