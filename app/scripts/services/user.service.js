angular.module("IdeaCanvas")
  .service("UserService", function(User, Auth, $http, $rootScope){
    var self = {
      login: function(credentials, cb){
        var req = Auth.login(credentials);
        self.promise(req, function (err, data) {
          console.log(err, data);
          cb(err, data);
        })
      },
      signup: function(account, cb){
        var req = User.save('', account);
        self.promise(req, function(err, data){

        });
      },
      promise: function(fn, cb){
        fn.$promise.then(cb);
      }
    };
    return self;
  });
