angular.module("IdeaCanvas")
  .config(function ($stateProvider, $urlRouterProvider, $sceProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: '/views/login.html',
          controller: 'LoginController'
        })
        .state('signup', {
          url: '/signup',
          templateUrl: '/views/signup.html',
          controller: 'SignupController'
        })
        .state('newidea', {
          url: '/new-idea',
          templateUrl: '/views/new-idea.html',
          controller: 'NewIdeaController'
        })
        .state('list', {
          url: '/list',
          templateUrl: '/views/list.html'
        });
      $urlRouterProvider.otherwise('/login');
  });
