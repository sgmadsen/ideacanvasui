angular.module('IdeaCanvas')
  .factory('Messages', function(){
    return {
      login: {
        error: {
          content: "Invalid!",
          action: "Misplaced your password?"
        },
        success: {
          content: "Success",
          action: "continue"
        }
      },
      condescend: {
        part1: "I hope this ",
        part2: " really knows what he's doing...",
        action: "trust em anyway"
      }
    }
  });
