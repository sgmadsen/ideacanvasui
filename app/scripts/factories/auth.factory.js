angular.module('IdeaCanvas')
  .factory('Auth', function($q, $rootScope, $resource){
    return $resource($rootScope.apiURL + 'auth/local', {},{
      'update': { method:'PUT' },
      'login'  : { method:'POST' }
    });
  });
