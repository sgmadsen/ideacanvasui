angular.module('IdeaCanvas')
    .factory('Idea', function($q, $rootScope, $resource){
      return $resource($rootScope.apiURL + 'idea/:id', {id: '@id'},{
			  'update': { method:'PUT' },
        'save'  : { method:'POST' }
		  });
    });
