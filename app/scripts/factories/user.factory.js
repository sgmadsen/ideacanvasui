angular.module('IdeaCanvas')
    .factory('User', function($q, $rootScope, $resource){
      return $resource($rootScope.apiURL + 'user/:id', {id: '@id'},{
			  'update': { method:'PUT' },
        'save'  : { method:'POST' }
		  });
    });
