angular.module("IdeaCanvas")
  .controller("NewIdeaController", function ($scope, $mdDialog, $rootScope, NavService, IdeaService) {
    NavService.setTabs(['info', 'pain', 'potential', 'prescription', 'Positioning', 'path']);
    NavService.showTabs();

    $scope.tabs = $rootScope.tabs;

    $scope.save = function () {
      IdeaService.create($rootScope.newIdea, function (err, data) {
        if(err) console.log(err);

        console.log(data);
      });
    };



    $scope.params = [

    ];

    $scope.params = {
      info: [      {
        label: "Inventor",
        type: "sub",
        key: "inventor",
        subkey: "name",
        val: ""
      },
        {
          label: "Labs Owner",
          type: "text",
          key: "labs_owner",
          val: ""
        },
        {
          label: "Labs Response",
          type: "text",
          key: "labs_response",
          val: ""
        }],
      pain: [{
        label: "Target Customer",
        type: "text",
        key: "target_customer",
        val: ""
      },
        {
          label: "Target Customer Job",
          type: "text",
          key: "target_customer_job",
          val: ""
        },
        {
          label: "Target Customer Pain",
          type: "slider",
          key: "target_customer_pain",
          val: ""
        },
        {
          label: "Pain Size",
          type: "slider",
          key: "pain_size",
          val: ""
        },
        {
          label: "Pain Frequency",
          type: "slider",
          key: "pain_frequency",
          val: ""
        }],
      potential: [{
        label: "Market Influences",
        type: "text",
        key: "market_influences",
        val: ""
      },
        {
          label: "Customer Count",
          type: "slider",
          key: "customer_count",
          val: ""
        },
        {
          label: "Sustainability",
          type: "slider",
          key: "sustainability",
          val: ""
        },
        {
          label: "Market Growth",
          type: "slider",
          key: "market_growth",
          val: ""
        },
        {
          label: "Optimism",
          type: "slider",
          key: "optimism",
          val: ""
        },
        {
          label: "Financial Potential",
          type: "slider",
          key: "financial_potential",
          val: ""
        }],
      prescription: [{
        label: "Solution Name",
        type: "text",
        key: "solution_name",
        val: ""
      },
        {
          label: "Solution Description",
          type: "text",
          key: "solution_description",
          val: ""
        },
        {
          label: "Differentiators",
          type: "text",
          key: "differentiators",
          val: ""
        },
        {
          label: "Innovation Name",
          type: "text",
          key: "innovation_name",
          val: ""
        },
        {
          label: "Distinction",
          type: "slider",
          key: "distinction",
          val: ""
        },
        {
          label: "Innovation",
          type: "slider",
          key: "innovation",
          val: ""
        },
        {
          label: "Experience",
          type: "slider",
          key: "experience",
          val: ""
        },
        {
          label: "Labs Platform",
          type: "slider",
          key: "labs_platform",
          val: ""
        }],
      positioning: [{
        label: "Gorilla",
        type: "text",
        key: "gorilla",
        val: ""
      },
        {
          label: "Casualties",
          type: "text",
          key: "casualties",
          val: ""
        },
        {
          label: "Market Landscape",
          type: "slider",
          key: "market_landscape",
          val: ""
        },
        {
          label: "Market Gorilla",
          type: "slider",
          key: "market_gorilla",
          val: ""
        },
        {
          label: "Entry Strategy",
          type: "slider",
          key: "entry_strategy",
          val: ""
        },
        {
          label: "Entry Strategy Description",
          type: "text",
          key: "entry_strategy_body",
          val: ""
        }]
    };

    $scope.selectedIndex = 0;
    $scope.next = function(){
      $rootScope.selectedIndex++;
    };
    $scope.prev = function(){
      $rootScope.selectedIndex--;
    }
  });
