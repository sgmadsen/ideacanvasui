angular.module("IdeaCanvas")
  .controller("DialogController", function ($scope, $mdDialog, $rootScope, Toast, $location, NavService) {
    $scope.params = [
      {
        label: "Inventor",
        key: "inventor",
        val: ""
      },
      {
        label: "Labs Owner",
        key: "labs_owner",
        val: ""
      },
      {
        label: "Labs Response",
        key: "labs_response",
        val: ""
      }
    ];
    $scope.next = function() {
      $mdDialog.hide();
      Toast.misc.condescend($rootScope.newIdea.inventor, function () {
        $location.path('/new-idea');
      });
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };

    $scope.hide = function () {
      NavService.hideTabs();
    }
    $scope.show = function () {
      NavService.showTabs();
    }
  });
