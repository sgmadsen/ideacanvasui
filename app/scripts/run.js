angular.module("IdeaCanvas")
  .run(function($rootScope){
    $rootScope.apiURL = "http://localhost:1337/";
    $rootScope.newIdea = {
      inventor: '',
      labs_owner: '',
      submitted: '',
      labs_response: '',
      pain: {
        
      },
      potential: [],
      prescription: []
    };

    $rootScope.tabs = ['info', 'pain', 'potential', 'prescription', 'positioning', 'path'];
  });
