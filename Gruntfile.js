'use strict';

module.exports = function (grunt) {
  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);
  var pkg = grunt.file.readJSON('contraventConfig.json');

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn'
  });
  grunt.loadNpmTasks('grunt-prompt');
  grunt.loadNpmTasks('grunt-bump');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-slack-hook');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-alert');


  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: pkg.appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all', 'newer:jscs:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
      },
      sass: {
        files: ['<%= yeoman.app %>/styles/{,**/}*.{scss,sass}'],
        tasks: ['sass:server', 'autoprefixer']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:css', 'postcss']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        sourcemap: true,
        loadPath: 'bower_components',
        dist: {
          files: {
            'main.css': 'main.scss'
          }
        }
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                  '/bower_components',
                  connect.static('./bower_components')
              ),
              connect().use(
                  '/src/css',
                  connect.static('./src/css')
              ),
              connect.static(pkg.appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                  '/bower_components',
                  connect.static('./bower_components')
              ),
              connect.static(pkg.appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Make sure code styles are up to par
    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },
    autoprefixer: {
      // prefix all files
      multiple_files: {
        expand: true,
        flatten: true,
        src: '*.css',
        dest: 'css/'
      }
    },
    // Add vendor prefixed styles
    postcss: {
      options: {
        processors: [
          require('autoprefixer-core')({browsers: ['last 1 version']})
        ]
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath:  /\.\.\//
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath:  /\.\.\//,
        fileTypes:{
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
            detect: {
              js: /'(.*\.js)'/gi
            },
            replace: {
              js: '\'{{filePath}}\','
            }
          }
        }
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/{,*/}*.js',
          '<%= yeoman.dist %>/styles/{,*/}*.css',
          //'<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
          '<%= yeoman.dist %>/styles/fonts/*'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/css'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
     cssmin: {
       dist: {
         files: {
           '<%= yeoman.dist %>/styles/main.css': [
             '.tmp/styles/{,*/}*.css',
             '<%= yeoman.app %>/styles/{,*/}*.css'
           ]
         }
       }
     },
     uglify: {
       dist: {
         files: {
           '<%= yeoman.dist %>/scripts/scripts.js': [
             '<%= yeoman.dist %>/scripts/scripts.js'
           ]
         }
       }
     },
     concat: {
       dist: {}
     },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'fidelitySummerStApp',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/scripts.js'
        },
        cwd: '<%= yeoman.app %>',
        src: 'views/{,*/}*.html',
        dest: '.tmp/templateCache.js'
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '*.html',
            'images/{,**/}*.*',
            'styles/fonts/{,*/}*.*',
            'assets/{,*/}*.*',
            'views/{,**/}*.html'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/css',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass:server',
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'sass',
        'copy:styles'
      ]
    },
    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    },
    aws_s3: {
      options: {
        accessKeyId: grunt.option( 'awsid' ),
        secretAccessKey:  grunt.option( 'awskey' ),
        progress: 'progressBar',

        region: 'us-west-2'
      },
      live: {
        options: {
          bucket: 'www.contravent.com'
          //params: {
          //  ContentEncoding: 'gzip'
          //}
        },
        files: [
          {expand: true, cwd: '<%= yeoman.dist %>', src: ['**'], dest: ''}
        ]
      },
      clean_live: {
        options: {
          bucket: 'www.contravent.com'
        },
        files: [
          {dest: '/', action: 'delete'}
        ]
      },
      dev: {
        options: {
          bucket: '<%= yeoman.servername %>-dev.contraventbuild.com'
          //params: {
          //  ContentEncoding: 'gzip'
          //}
        },
        files: [
          {expand: true, cwd: '<%= yeoman.dist %>', src: ['**'], dest: ''}
        ]
      },
      clean_dev: {
        options: {
          bucket: '<%= yeoman.servername %>-dev.contraventbuild.com'
        },
        files: [
          {dest: '/', action: 'delete'}
        ]
      }
    },
    slack: {
      options: {
        endpoint: '<%= yeoman.slackURL %>',
        channel: '#<%= yeoman.slackChannel %>'
      },
      live: {
        text: 'Live has been successfully deployed to http://www.contravent.com by <%= developerName %> '
      },
      dev: {
        text: 'Dev has been successfully deployed to http://<%= yeoman.servername %>-dev.contraventbuild.com by <%= developerName %>'
      }
    },

    prompt: {
      commitmessage: {
        options: {
          questions: [
            {
              config:  'commitmessage',
              type: 'input',
              message: 'What is your commit message?',
              default: ''
            }
          ]
        }
      },
      committype: {
        options: {
          questions: [
            {
              config:  'committype',
              type: 'list',
              message: 'Commit type?',
              default: 'dev',
              choices: ['dev', 'alpha', 'beta', 'rc']
            }
          ]
        }
      },
      deployto: {
        options: {
          questions: [
            {
              config:  'deployto',
              type: 'list',
              message: 'Deploy type?',
              default: 'dev',
              choices: ['dev', 'live']
            }
          ]
        }
      },
    },
    bump: {
      options: {
        files: ['contraventConfig.json'],
        updateConfigs: [],
        commit: true,
        commitMessage: '%VERSION%    <%= grunt.config("commitmessage") %>',
        commitFiles: ['-a'],
        createTag: true,
        tagName: 'v%VERSION%',
        tagMessage: 'v%VERSION%',
        push: true,
        pushTo: 'origin',
        gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
        globalReplace: false,
        prereleaseName: '<%= grunt.config("committype") %>',
        metadata: '',
        regExp: false
      }
    },

    notify: {
      task_name: {
        options: {
          title: '<%= yeoman.servername %>'
        }
      },
      watch: {
        options: {
          title: 'Task Complete',  // optional
          message: 'SASS and Uglify finished running', //required
        }
      },
      server: {
        options: {
          message: 'Server is ready!'
        }
      },
      deploy: {
        options: {
          message: 'App has been deployed'
        }
      },
      bower: {
        options: {
          message: 'bower install ran'
        }
      }
    },

    bower: {
      install: {

      }
    },

    alert: {
      slack: {
        type: 'slack',
        webhookUrl: '<%= yeoman.slackURL %>',
        channel: '#<%= yeoman.slackChannel %>', //optional
        username: 'Grunt Alert', //optional
        iconEmoji: ':bangbang:', //optional
        message: 'Build just failed with this error: %s'
      }
    }

  });

  grunt.registerTask('username', '',
      function () {
        var done = this.async();
        grunt.util.spawn({
          cmd: 'finger',
          args: ['-lp']
        }, function (err, result) {
          var username = '';
          var output = result.stdout;
          if (output) {
            var matches = result.stdout.match(/Login: ([a-zA-Z0-9]+)[\s\t]+Name: ([a-zA-Z0-9 ]+)/);
            var name = matches[2];
            username = (name );
          }

          if(username === ''){
            username = 'Bamboo Deploy';
          }

          grunt.config('developerName', username);
          done();
        });
      });




  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'username',
      'bower:install',
      'clean:server',
      'wiredep',
      'concurrent:server',
      'postcss:server',
      'connect:livereload',
      // 'notify:server',
      'watch'

    ]);
  });

  grunt.registerTask('test', [
    'username',
    'clean:server',
    'wiredep',
    'concurrent:test',
    'postcss',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'username',
    'clean:dist',
    'wiredep',
    'useminPrepare',
    'concurrent:dist',
    'postcss',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
    'htmlmin'
    //'bump'
  ]);

  grunt.registerTask('default', [
    'username',
    'newer:jshint',
    'newer:jscs',
    'test',
    'build'
  ]);

  grunt.registerTask('live', [
    'build'
  ]);

  grunt.registerTask('live:push', [
    'username',
    'prompt:committype',
    'prompt:commitmessage',
    'bump:prerelease',
    'aws_s3:clean_live',
    'aws_s3:live',
    'slack:live',
    'notify:deploy'
  ]);

  grunt.registerTask('live:pushonly', [
    'username',
    'aws_s3:clean_live',
    'aws_s3:live',
    'slack:live',
    'notify:deploy'
  ]);

  grunt.registerTask('dev', [
    'username',
    'build'
  ]);

  grunt.registerTask('dev:push', [
    'username',
    'prompt:committype',
    'prompt:commitmessage',
    'bump:prerelease',
    'aws_s3:clean_dev',
    'aws_s3:dev',
    'slack:dev',
    'notify:deploy'
  ]);

  grunt.registerTask('dev:pushonly', [
    'username',
    'aws_s3:clean_dev',
    'aws_s3:dev',
    'slack:dev',
    'notify:deploy'
  ]);


  grunt.registerTask('commit', [
    'username',
    'prompt:committype',
    'prompt:commitmessage',
    'bump:prerelease'
  ]);





};